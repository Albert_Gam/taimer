window.onload = function() {
	document.getElementById("Secret").click();
};

// переменная, которая отслеживает, сколько секунд прошло
var now_seconds = 0;
// var now_times = 0;


// переменная для отсчёта интервалов
var intervalVariable;

// переменная, которая следит за количеством секунд в таймерах
var seconds_1 = 0;

// ставим начальные зачение счётчиков
var timer_minutes = 0;
var timer_seconds = 0;

// подготовка звукового оповещения
function audio_change() {
	
	// находим аудиоэлемент на странице
	var audio = $('#audio_beep');

	// подключаем нужный файл со звуком. Файлов два, потому что разные браузеры играют разные форматы
	$('#audio_beep source[type="audio/ogg"]').attr('src', 's.ogg');
	$('#audio_beep source[type="audio/mp3"]').attr('src', 's.mp3');

	// ставим звук на паузу и подгружаем его
	audio[0].pause();
	audio[0].load();
}

// функция, которая отвечает за смену времени на таймере
function timerTick(type, timer_params) {

	now_seconds++;

		if(interval_type == 'work') {

			
			if(timer_params.time_work - now_seconds > 0) {

				renderTimerNums(timer_params.time_work - now_seconds);
			} 

			else {
				$('#minus').text('-');
				// обнуляем табло
				renderTimerNums(0);

				// проигрываем звук уведомления
				$('#audio_beep')[0].play();

				now_seconds = 0;

				interval_type = 'rest';

				// меняем цвет цифр на таймере
				$('.timer_panel_nums .timer_nums').removeClass('green');
				$('.timer_panel_nums .timer_nums').addClass('red');
			}
		} 

		else if(interval_type == 'rest') {

			if(timer_params.time_rest - now_seconds > 0) {
				

				renderTimerNums(timer_params.time_rest - now_seconds);				
			} 

			else {
				$('#minus').text('');
				// обнуляем табло
				renderTimerNums(0);

				// проигрываем звук уведомления
				$('#audio_beep')[0].play();

				// обнуляем секунды
				now_seconds = 0;

				now_times++;

				if(now_times > timer_params.interval_count) {

					// обновляем информацию на панели настроек
					$('.timer_interval_nums.times').text(timer_params.interval_count);
					$('#timer_pause').trigger('click');
					now_seconds = 0;
					$('.timer_panel_nums .timer_nums').removeClass('red');
				} 

				else {

					// обновляем информацию на панели настроек
					$('.timer_interval_nums.times').text(now_times);
					$('.timer_panel_nums .timer_nums').removeClass('red');
					$('.timer_panel_nums .timer_nums').addClass('green');
				}

				interval_type = 'work';
			}
		}
	
}

// разбиваем секунды на часы, минуты и секунды
function secondsToTime(seconds) {
	var h = parseInt(seconds / 3600 % 24);
	var m = parseInt(seconds /  60 % 60);
	var s = parseInt(seconds % 60);
	return {'hours': leadZero(parseInt(h)), 'minutes': leadZero(parseInt(m)), 'seconds': leadZero(parseInt(s))};
}

// для красоты ставим первым ноль, если число минут или секунд меньше девяти
function leadZero(num) {
	var s = "" + num;
	if (s.length < 2) {
		s = "0" + s ;
	}
	return s;
}

// отображение времени на табло
function renderTimerNums(seconds) {
	var timer_nums = secondsToTime(seconds)
	$('.timer_nums.minutes').text(timer_nums.minutes);
	$('.timer_nums.seconds').text(timer_nums.seconds);
}

// стартовый скрипт, который подготавливает интервальный таймер к первому запуску
$('.timer_types_btn').click(function() {
	$('.timer_types_btn').removeClass('active');
	$(this).addClass('active');
	$('#timer_pause').trigger('click', {audio: 0});

	interval_type = 'work';
	$('.timer_panel_nums .timer_nums').removeClass('green red');
	timer_minutes = $('.timer_nums.minutes').text();
	timer_seconds = $('.timer_nums.seconds').text();

	// делаем панель настроек видимой 
	$('.timer_panel_info').removeClass('hide');
	$('.timer_panel_nums .timer_sep').eq(0).addClass('hide');
	
});

// обрабатываем нажатие на кнопку «Старт»
$('#timer_run').click(function() {

	// записываем установленные минуты и секунды
	timer_minutes = $('.timer_nums.minutes').text();
	timer_seconds = $('.timer_nums.seconds').text();	

	// переменная, которая будет хранить параметры интервального цикла
	var timer_params = {};	
	
	timer_params.time_work = $('.timer_interval_work .minutes').text()*60 + $('.timer_interval_work .seconds').text()*1;
	if(timer_params.time_work==0){
		alert ('Задайте интервал')
	}
	else{
	// настраиваем аудио
	audio_change();

	// делаем кнопку «Старт» невидимой
	$(this).addClass('hide');	
	// а кнопку «Пауза» — наоборот, видимой
	$('#timer_pause').removeClass('hide');
	// запускаем звуковое оповещение
	$('#audio_beep')[0].play();
	timer_params.time_rest = $('.timer_interval_rest .minutes').text()*60 + $('.timer_interval_rest .seconds').text()*1;
	timer_params.interval_count = $('.timer_interval_count .all_times').text()*1;

	now_times = $('.timer_interval_count .times').text()*1;

	if(now_times >= timer_params.interval_count) {
		now_times = 1;
		$('.timer_interval_count .times').text(now_times);
	}

	if(interval_type == 'work') {
		$('.timer_panel_nums .timer_nums').addClass('green');
		seconds_1 = timer_params.time_work;
	} 

	else if(interval_type == 'rest') {		
		$('.timer_panel_nums .timer_nums').addClass('red');		
		seconds_1 = timer_params.time_rest;		
	}

	intervalVariable = setInterval(timerTick, 1000, 'interval', timer_params);
	}	
	// выходим из функции 
	return false;
});

// что будет, если мы нажмём на кнопку «Пауза»
$('#timer_pause').click(function(event, params) {

	// если кнопка сработала нормально
	if(params !== undefined) {

		// но аудиопараметры ещё не задавались
		if(params.audio === undefined) {

			// задаём параметры оповещения
			params.audio = 1;
		}
	} 

	// иначе сразу задаём параметры звука
	else {
		params = {audio: 1};
	}

	// после нажатия на кнопку «Пауза» делаем её невидимой
	$(this).addClass('hide');

	// а кнопку «Старт» — наоборот, видимой
	$('#timer_run').removeClass('hide');

	// останавливаем таймер
	clearInterval(intervalVariable);

	// если со звуком всё в порядке
	if(params.audio) {

		// проигрываем звуковое оповещение
		$('#audio_beep')[0].play();
	}

	// выходим из функции
	return false;
});

// что будет, если нажмём на кнопку «Сброс»
$('#timer_clear').click(function() {

	// имитируем нажатие на кнопку «Пауза», чтобы остановить таймер
	$('#timer_pause').trigger('click', {audio: 0});

	// убираем красный цвет на табло, которое там осталось от включения паузы
	$('.timer_panel_nums .timer_nums').removeClass('green red');

	// обнуляем табло
	renderTimerNums(0);

	// обнуляем значения служебных переменных
	now_seconds = 0;
	now_times = 0;

	// задаём начальное значение первого блока секунд
	seconds_1 = 00;

	// устанавливаем начальные значения времени тренировки, отдыха и количества повторов
	$('.timer_interval_work .minutes').text('00');
	$('.timer_interval_work .seconds').text('00');
	$('.timer_interval_rest .minutes').text('00');
	$('.timer_interval_rest .seconds').text('05');
	
	// выходим из функции
	return false;
});